import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Link, usePaginatedQuery, useRouter, BlitzPage } from "blitz"
import getTodos from "app/todos/queries/getTodos"

const ITEMS_PER_PAGE = 100

export const TodosList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ todos, hasMore }] = usePaginatedQuery(getTodos, {
    orderBy: { id: "asc" },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })

  const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
  const goToNextPage = () => router.push({ query: { page: page + 1 } })

  return (
    <div>
      <p>
        back to{" "}
        <Link href="/">
          <a>home</a>
        </Link>
      </p>
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>
            <Link href="/todos/[todoId]" as={`/todos/${todo.id}`}>
              <a>{todo.name}</a>
            </Link>
          </li>
        ))}
      </ul>

      <button disabled={page === 0} onClick={goToPreviousPage}>
        Previous
      </button>
      <button disabled={!hasMore} onClick={goToNextPage}>
        Next
      </button>
    </div>
  )
}

const TodosPage: BlitzPage = () => {
  return (
    <div>
      <p>
        <Link href="/todos/new">
          <a>Create Todo</a>
        </Link>
      </p>

      <Suspense fallback={<div>Loading...</div>}>
        <TodosList />
      </Suspense>
    </div>
  )
}

TodosPage.getLayout = (page) => <Layout title={"Todos"}>{page}</Layout>

export default TodosPage
