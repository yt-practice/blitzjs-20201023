import React from "react"
import { LabeledTextField } from "app/components/LabeledTextField"
import { Form, FORM_ERROR } from "app/components/Form"
import * as z from "zod"
import { Todo } from "db"

const TodoInput = z.object({
  name: z.string(),
  dueAt: z.string(),
})

type TodoInputType = typeof TodoInput

type TodoFormProps = {
  initialValues: Partial<Pick<Todo, "name" | "dueAt">>
  onSubmit: (value: z.infer<TodoInputType>) => Promise<void> | void
  submitText: string
}

const TodoForm = ({ initialValues, onSubmit, submitText }: TodoFormProps) => {
  const init = {
    ...initialValues,
    dueAt:
      initialValues.dueAt &&
      [
        initialValues.dueAt.getFullYear(),
        String(initialValues.dueAt.getMonth() + 1).padStart(2, "0"),
        String(initialValues.dueAt.getDate()).padStart(2, "0"),
      ].join("-") +
        "T" +
        [
          String(initialValues.dueAt.getHours()).padStart(2, "0"),
          String(initialValues.dueAt.getMinutes()).padStart(2, "0"),
        ].join(":"),
  }

  return (
    <Form<TodoInputType>
      submitText={submitText}
      schema={TodoInput}
      initialValues={init}
      onSubmit={async (event) => {
        try {
          onSubmit(event)
        } catch (error) {
          return { [FORM_ERROR]: error.toString() }
        }
      }}
    >
      <LabeledTextField name="name" label="name" />
      <LabeledTextField name="dueAt" label="dueAt" type="datetime-local" />
    </Form>
  )
}

export default TodoForm
