import * as z from "zod"

export const CreateInput = z.object({
  name: z.string(),
  dueAt: z.date(),
})
export type CreateInputType = z.infer<typeof CreateInput>

export const UpdateInput = z.object({
  name: z.string(),
  dueAt: z.date(),
})
export type UpdateInputType = z.infer<typeof UpdateInput>
