import { Ctx } from "blitz"
import db from "db"
import { CreateInput, CreateInputType } from "../validations"

type CreateTodoInput = { data: CreateInputType }
export default async function createTodo({ data }: CreateTodoInput, ctx: Ctx) {
  ctx.session.authorize()

  const { name, dueAt } = CreateInput.parse(data)

  const todo = await db.todo.create({
    data: {
      name,
      dueAt,
      user: { connect: { id: ctx.session.userId } },
    },
  })

  return todo
}
