import { Ctx } from "blitz"
import db, { Todo } from "db"

type DeleteTodoInput = { id: Todo["id"] }

export default async function deleteTodo({ id }: DeleteTodoInput, ctx: Ctx) {
  ctx.session.authorize()

  const where = { id, user: { connect: { id: ctx.session.userId } } }
  const todo = await db.todo.delete({ where })

  return todo
}
