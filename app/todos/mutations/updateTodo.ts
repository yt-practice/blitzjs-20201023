import { Ctx } from "blitz"
import db, { Todo } from "db"
import { UpdateInput, UpdateInputType } from "../validations"

type UpdateTodoInput = {
  id: Todo["id"]
  data: UpdateInputType
}

export default async function updateTodo({ id, data }: UpdateTodoInput, ctx: Ctx) {
  ctx.session.authorize()

  const { name, dueAt } = UpdateInput.parse(data)
  const where = { id, user: { connect: { id: ctx.session.userId } } }

  const todo = await db.todo.update({ where, data: { name, dueAt } })

  return todo
}
